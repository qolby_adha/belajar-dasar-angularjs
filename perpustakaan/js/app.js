
var myController = function ($scope) {
    // $scope.page = "Book Page"
};
var bookCtrl = function ($scope) {
    $scope.page = "Book Page"
};

var perpustakaanConfig = function ($stateProvider, $urlRouterProvider) {
    var myState = {
        home: {
            name: 'home',
            url: '/',
            template: '<h1>This Is Home</h1>'
        },
        about: {
            name: 'about',
            url: '/about',
            template: '<h1>This Is About</h1>'
        },
        author: {
            name: 'author',
            url: '/author',
            templateUrl: 'template/author.html'
        },
        book: {
            name: 'book',
            url: '/book',
            templateUrl: 'template/book.html',
            controller: 'book-ctrl'
        }
    };

    $stateProvider
        .state(myState.home)
        .state(myState.about)
        .state(myState.author)
        .state(myState.book);

    $urlRouterProvider
        .outherwise("/");
};

angular.module('perpustakaan-app', ["ui.router"])
    .config(perpustakaanConfig)
    .controller('my-controller', myController)
    .controller('book-ctrl', bookCtrl);