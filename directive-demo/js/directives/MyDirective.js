// example directive Scope
// app.directive('myDirective', function () {
//     return {
//         scope: {
//             // dunia : "Hello Saya Isolated Scope"
//         },
//         restrict: 'EA',
//         template: '<h2> Hello {{dunia}}</h2>',
//         replace: true,
//         link : function(scope,elem,attrs){
//             elem.bind('click',function(){
//                 scope.dunia='Saya diklik';
//                 scope.$digest();
//             });
//         }
//     };
// });

// example direcetive one way banding
// app.directive('oneWay',function(){
//     return {
//         restrict:'E',
//         scope:{
//             title:'@'
//         },
//         template:'<h2>Title Inside : {{title}}</h2>',
//         link : function(scope,elem,attrs){
//             elem.bind('click',function(){
//                 scope.$apply(function() {
//                     scope.title = "JavaScript";
//                 });
//             });
//         }
//     };
// });


// example directive two way banding
app.directive('oneWay',function(){
    return {
        restrict:'E',
        scope:{
            title:'='
        },
        template:'<h2>Title Inside : {{title}}</h2>',
        link : function(scope,elem,attrs){
            elem.bind('click',function(){
                scope.$apply(function() {
                    scope.title = "JavaScript";
                });
            });
        }
    };
});
