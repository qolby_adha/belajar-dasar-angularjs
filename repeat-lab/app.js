let app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope) {
    $scope.categories = ['Nama', 'Kelas', 'Nilai'];

    $scope.ordering = function (ordvar, by) {
        ordvar = !ordvar;
        $scope.ordstatus = ordvar;
        $scope.ord = by;
        return ordvar;
    }
    $scope.students = [
        { name : "jajan", class: "11", score : 12 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "12", score : 14 },
        { name : "jajan", class: "12", score : 15 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "11", score : 14 },
        { name : "jajan", class: "13", score : 15 },
        { name : "jajan", class: "11", score : 13 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "12", score : 10 },
        { name : "jajan", class: "11", score : 12 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "12", score : 14 },
        { name : "jajan", class: "12", score : 15 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "11", score : 14 },
        { name : "jajan", class: "13", score : 15 },
        { name : "jajan", class: "11", score : 13 },
        { name : "jajan", class: "12", score : 13 },
        { name : "jajan", class: "12", score : 10 },
    ];
});