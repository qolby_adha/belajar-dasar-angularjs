let app = angular.module('xapp', []);

app.controller('xctrl', function ($scope) {
    $scope.operate = function (value) {
        let inputA = +$scope.obj1;
        let inputB = +$scope.obj2;
        switch (value) {
            case 1:
                $scope.operation = "ADD";
                $scope.result = inputA + inputB;
                break;
            case 2:
                $scope.operation = "SUBSTRATION";
                $scope.result = inputA - inputB;
                break;
            case 3:
                $scope.operation = "MULTIPLY";
                $scope.result = inputA * inputB;
                break;
            case 4:
                $scope.operation = "DIVIDE";
                $scope.result = inputA / inputB;
                break;
        }
    }
});