let app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope) {
    // $scope.categories = ['Nama', 'Kelas', 'Nilai'];

    $scope.students = [
        { name: "2jajan", class: "11", score: 12 },
        { name: "3jajan", class: "12", score: 13 },
        { name: "4jajan", class: "12", score: 13 },
        { name: "5jajan", class: "12", score: 14 },
        { name: "6jajan", class: "12", score: 15 },
        { name: "7jajan", class: "12", score: 13 },
        { name: "8jajan", class: "11", score: 14 },
        { name: "9jajan", class: "13", score: 15 },
        { name: "0jajan", class: "11", score: 13 },
        { name: "1jajan", class: "12", score: 13 },
        { name: "11jajan", class: "12", score: 10 },
        { name: "12jajan", class: "11", score: 12 },
        { name: "14jajan", class: "12", score: 13 },
        { name: "13jajan", class: "12", score: 13 },
        { name: "16jajan", class: "12", score: 14 },
        { name: "15jajan", class: "12", score: 15 },
        { name: "17jajan", class: "12", score: 13 },
        { name: "18jajan", class: "11", score: 14 },
        { name: "19jajan", class: "13", score: 15 },
        { name: "20jajan", class: "11", score: 13 },
        { name: "21jajan", class: "12", score: 13 },
        { name: "22jajan", class: "12", score: 10 },
    ];
    $scope.pagesizes = [5,10,15,20];
    $scope.pagesize = $scope.pagesizes[0];
    $scope.currentpage = 0;
    $scope.pagenumber = Math.ceil($scope.students.length / $scope.pagesize);


    $scope.paging = function (type) {
        if (type == 0 && $scope.currentpage > 0) {
            --$scope.currentpage;
        } else if (type == 1 && $scope.currentpage < $scope.pagenumber - 1) {
            ++$scope.currentpage;
        }
    }

    $scope.changeAction = function (p) {
        $scope.pagesize = p;
        $scope.currentpage = 0;
        $scope.pagenumber = Math.ceil($scope.results.length / $scope.pagesize);
    }

    $scope.getNumber = function (n) {
        return new Array(n)
    }
    $scope.getPage = function (i) {
        $scope.currentpage = i;

    }

    $scope.$watchCollection('results', function () {
        if ($scope.results === undefined) return;
        $scope.currentpage = 0;
        $scope.pagenumber = Math.ceil($scope.results.length / $scope.pagesize);
    });

    $scope.ordering = function (ordvar, by) {
        ordvar = !ordvar;
        $scope.ordstatus = ordvar;
        $scope.ord = by;
        return ordvar;
    }
});

